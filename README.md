# Montagnard
(Description to come) 

## Table of Contents
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Running the app](#running-the-app)
- [Running the Tests](#running-the-tests)
- [Folder Structure](#folder-structure)
- [Branching](#branching)
- [Built With](#built-with)
- [Versioning](#versioning)
- [Authors](#authors)
- [Acknowledgements](#acknowledgments)

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites
The following will need to be installed on your local environment:
- [Node.js](https://nodejs.org/en/) 
- [npm](https://www.npmjs.com/)
- An IOS or Andriod Emulator

### Running the app
Run the application on your local computer. 

Steps:
1. Install the dependencies  
    a. In the terminal navigate to the root folder of the project    
    b. Run *npm install*  
2. Start the app
    a. In the terminal navigate to the root folder of the project
    a. Run *npm start*

Expo developer tools should automatically open up on [http://localhost:19002/](http://localhost:19002/). In the expo developer tools you can choose to Run the app on an Andriod Emulator or IOS Simulator. If selecting one of these options does not open up the Emulator automatically you may need to manually open up the Emulator on your computer and then reselect the option to run the app on the Emulator.

Expo also provides the option of running the app on your physical phone. To do this you need to download the Expo app onto your phone then open it up and scan the barcode that is displayed in the expo developer tools. 

Note also that the app is started with hot reloading meaning that when you make changes to the code you should see those changes reflected in the app without needing to restart it.

## Running the tests
The unit tests use [Jest](https://jestjs.io/).

To execute the tests run: *npm test*

Optionally, you can install a Jest plugin for your IDE of choice that lets you to run the tests through the IDE. One of the better options is to work in Visual Studio Code and use the [VSCode Jest plugin](https://github.com/jest-community/vscode-jest).

## Folder Structure
The basic structure is:
```
├── node_modules         
├── docs                        // contains the documentation for the app
├── src
│   ├── components              // components stored in this folder can be used anywhere in the application
│   │     ├─ List 
│   │     └─ Button
│   │         ├─ components     // components found in nested component folders can only be used by direct parents
│   │         └─ Button.tsx
│   │
│   ├── scenes                  // scenes are pages in the application, 
│   │     │                     // anything defined within a scene folder can only be used within that folder
│   │     ├─ SignIn     
│   │     └─ LandingPage
│   │         ├─ components
│   │         └─ LandingPage.tsx
│   │ 
│   └── services                // services are self contained modules that contain business logic for the appication
│         ├─ api
│         └─ state
│             ├─ actions
│             ├─ reducers
│             └─ store.ts
│ 
└── package.json
```

The motivation for the structure of the src file is based on the medium article [How to better organize your React applications?](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1) 
Before adding any folders to this application you should give the article a quick read through.

## Branching 
This project uses the [gitflow branching](https://medium.com/@muneebsajjad/git-flow-explained-quick-and-simple-7a753313572f) pattern. 

When creating a personal branch off of a feature branch the following name pattern should be used:
*{developers-name}/{name-of-feature-branched-off-of}/{branch-description}*. For example if I branched off of the 
feature branch *feature/initial-construct* to implement some code for adding redux for the application then I would make a branch
with named *eric/feature/initial-construct/adding-redux*.

## Built With
- [React Native](https://facebook.github.io/react-native/)
- [React](https://reactjs.org/)
- [Expo](https://expo.io/)
- [Redux](https://redux.js.org/)
- [React-Redux](https://github.com/reduxjs/react-redux)
- [React Navigation](https://reactnavigation.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Node](https://nodejs.org/en/)
- [Jest](https://jestjs.io/)

## Versioning
We use [SemVer](http://semver.org/) for versioning.

## Authors
- Eric Lammers (ericjameslammers@gmail.com)

## Acknowledgments
- [Alexis Mangin](https://medium.com/@alexmngn) author of [How to better organize your React applications?](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1) 
