import React from 'react';
import { Provider } from 'react-redux';
import { store } from './src/service/state/store';
import MainPage from './src/scene/MainPage/MainPage';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import SignIn from './src/scene/SignIn/SignIn';
import Account from './src/scene/Account/Account';
import Register from './src/scene/Register/Register';
import Landing from './src/scene/Landing/Landing';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { ROUTES } from './src/service/navigation/routes';

const AppNavigator = createStackNavigator(
  {
    MainPage,
    SignIn,
    Account,
    Register,
    Landing
  },
  {
    initialRouteName: ROUTES.Account,
  }
);

const AppContainer = createAppContainer(AppNavigator);

class App extends React.Component {
  async componentDidMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
  }

  render() {
    return (
      <Provider store={store} >
        <AppContainer />
      </Provider>
    );
  }
};

export default App;