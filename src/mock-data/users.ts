import { User } from '../service/requests/user';

class Users {
   static UserData: Map<String, User> = new Map()
      .set(
         'EricLammers',
         {
            userName: 'EricLammers',
            password: 'Password',
            email: 'ericlammers199@gmail.com',
            phoneNumber: 5294353485,
            birthday: new Date('19931204')
         }
      );

   static addUser(user: User) {
      this.UserData.set(user.userName, user);
   }

   static getUser(userName: string) : User {
      return this.UserData.get(userName);
   }
}



export default Users;

