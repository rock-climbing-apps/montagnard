import * as React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

const buttonStyles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        padding: 10,
        paddingRight: 20,
        paddingLeft: 20,
        borderWidth: 2,
        borderColor: 'white',
        borderRadius: 5,
        marginBottom: 20,
        width: 250,
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white',
    }
});

interface ButtonProps {
    text: string,
    onPress: any,
    disabled?: boolean,
    buttonStyle?: object,
}

const WhiteButton = ({ text, onPress, buttonStyle, disabled }: ButtonProps) => {
    const getButtonOpacityStyle = () => ({
        opacity: disabled ? .2 : 1
    });

    return (
        <TouchableOpacity
            onPress={onPress}
            disabled={disabled}
            style={[buttonStyles.container, buttonStyle, getButtonOpacityStyle()]}
        >
            <Text style={buttonStyles.text}>{text}</Text>
        </TouchableOpacity>
    );
}

export default WhiteButton;