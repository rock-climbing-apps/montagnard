import * as React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';
import { ReactNode } from 'react';

const remote = 'https://images.pexels.com/photos/946337/pexels-photo-946337.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

interface BackgroundClimberProps {
    children: ReactNode
}

const BackgroundClimber = ({children} : BackgroundClimberProps) => (
    <ImageBackground style={styles.container} source={{ uri: remote }}>
        {children}
    </ImageBackground>
);

export default BackgroundClimber;
