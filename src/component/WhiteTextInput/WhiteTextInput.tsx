import * as React from 'react';
import { TextInput, StyleSheet, TextStyle } from 'react-native';

const styles = StyleSheet.create({
    textInput: {
        fontSize: 25,
        margin: 8,
        width: 300,
        backgroundColor: 'white',
        textAlign: 'center',
        padding: 10,
        paddingRight: 20,
        paddingLeft: 20,
        borderColor: 'black',
        borderRadius: 5,
    },
});

interface TextProps {
    onChangeText : any
    placeholder : string
    style ?: TextStyle
    editable?: boolean
}

const WhiteTextInput = ({placeholder, onChangeText, style, editable} : TextProps) => {
    const getTextInputStyle = () : Array<TextStyle> => ([
        styles.textInput,
        {
            opacity: !editable ? .5 : 1
        },
        style
    ]);

    return (
        <TextInput 
            placeholder={placeholder}
            style={getTextInputStyle()}
            onChangeText={onChangeText}
            autoCapitalize='none'
            editable={editable}
        />
    );
}

export default WhiteTextInput;



