export enum ROUTES {
    MainPage = "MainPage",
    SignIn = "SignIn",
    Account = "Account",
    Register = "Register",
    Landing = 'Landing'
}