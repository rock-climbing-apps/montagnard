import { AppState } from './store';
import * as userSelectors from './user/selector';

export const loggedIn = (state: AppState) => userSelectors.loggedIn(state.user);

export const loggingIn = (state: AppState) => userSelectors.loggingIn(state.user);

