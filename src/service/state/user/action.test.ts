import { loginUser, logoutUser, startLogin } from './actions';
import { UserActionTypes, LOGIN_USER, LOGOUT_USER, User, START_LOGIN } from './types';

describe('User Action Creators', () => {
    it('loginUser', () => {
       // TODO - Test the loginUser action creator
    });

    it('logoutUser', () => {
        const expectedAction: UserActionTypes = {
            type: LOGOUT_USER,
        }; 

        expect(logoutUser()).toEqual(expectedAction);
    });

    it('startLogin', () => {
        const userName = 'EricLammers';
        const expectedAction: UserActionTypes = {
            type: START_LOGIN,
            userName
        }; 

        expect(startLogin(userName)).toEqual(expectedAction);
    });
});
