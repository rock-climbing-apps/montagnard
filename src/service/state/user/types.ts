export interface User {
    session: string
    userName: string
}

export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';
export const START_LOGIN = 'LOGGING_IN';
export const LOGIN_FAILED = "LOGIN_FAILED";

interface LoginUserAction {
  type: typeof LOGIN_USER
  user: User
}

interface LogoutUserAction {
  type: typeof LOGOUT_USER
}

interface StartLoginAction {
  type: typeof START_LOGIN
  userName: string
}

interface LoginFailedAction {
  type: typeof LOGIN_FAILED
}

export type UserActionTypes = LoginUserAction | LogoutUserAction | StartLoginAction | LoginFailedAction;
