import { Dispatch } from 'redux';
import { User, LOGIN_USER, LOGOUT_USER, START_LOGIN, LOGIN_FAILED, UserActionTypes } from './types'
import { UserLoginInfo, authenticate } from '../../requests/user';
import { ROUTES } from '../../navigation/routes';

export const loginUserActionCreator = (user: User): UserActionTypes => ({
    type: LOGIN_USER,
    user: user,
})

export const startLogin = (userName: string): UserActionTypes => ({
    type: START_LOGIN,
    userName: userName
});

export const loginFailed = () : UserActionTypes => ({
    type: LOGIN_FAILED,
});

export const loginUser = (userLoginInfo : UserLoginInfo, navigation : any) : any => {
    return async (dispatch : Dispatch) => {
        dispatch(startLogin(userLoginInfo.userName));
        
        const onSuccess = (user: User) : UserActionTypes => {
            navigation.navigate(ROUTES.Landing);
            return dispatch(loginUserActionCreator(user));
        }

        const onError = () : UserActionTypes => dispatch(loginFailed());

        authenticate(userLoginInfo, onSuccess, onError);
    }
}

export const logoutUser = (): UserActionTypes => ({
    type: LOGOUT_USER,
});

