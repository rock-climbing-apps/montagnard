import { User } from './types';

export const loggedIn = (userState : User) : boolean => {
    return userState.userName !== null && userState.session !== null;
}

export const loggingIn = (userState : User) : boolean => {
    return userState.userName != null && userState.session === null;
}