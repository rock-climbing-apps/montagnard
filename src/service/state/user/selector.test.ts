import { User } from "./types";
import { initialState } from "./reducer";
import { loggedIn, loggingIn } from './selector';

describe('User Selectors', () => {
    describe('Logged In Selector', () => {
        it('Returns true if the user states session and username is set', () => {
            const userState: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'Eric Lammers'
            };

            expect(loggedIn(userState)).toBeTruthy();
        });

        it('Returns false for the initial state', () => {
            expect(loggedIn(initialState)).toBeFalsy();
        });

        it('Returns false if the username is not set', () => {
            const userState: User = {
                session: null,
                userName: 'Eric Lammers'
            };

            expect(loggedIn(userState)).toBeFalsy();
        });

        it('Returns false if the session is not set', () => {
            const userState: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: null
            };

            expect(loggedIn(userState)).toBeFalsy();
        });
    });

    describe('Logging In Selector', () => {
        it('Returns true if the username set but the session is still null', () => {
            const userState: User = {
                session: null,
                userName: 'Eric Lammers'
            };

            expect(loggingIn(userState)).toBeTruthy();
        });

        it('Returns false for the initial state', () => {
            expect(loggedIn(initialState)).toBeFalsy();
        });

        it('Returns false if the session and username are set', () => {
            const userState: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'EricLammers'
            };

            expect(loggingIn(userState)).toBeFalsy();
        });
    });
});