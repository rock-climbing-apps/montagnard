import { User } from "./types";
import { userReducer, initialState } from "./reducer";
import { loginUserActionCreator, logoutUser, startLogin } from "./actions";

describe('User Reducer', () => {
    describe('Login User Action', () => {
        it('Updates the state to contain the logged in user when passed the login action', () => {
            const user: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'Eric Lammers'
            };

            const resultState = userReducer(initialState, loginUserActionCreator(user));
        
            expect(resultState).toEqual(user);
        });
    });

    describe('Start Login Action', () => {
        it('Sets the user name to the provided username and the session to null', () => {
            const user: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'Eric Lammers'
            };

            const userName = 'Tony'
            const resultState = userReducer(user, startLogin(userName));
    
            expect(resultState).toEqual({
                userName: userName,
                session: null
            });
        });
    });

    describe('Logout User Action', () => {
        it('Logouts the user when passed the logout action', () => {
            const user: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'Eric Lammers'
            };

            const resultState = userReducer(user, logoutUser());

            expect(resultState).toEqual(initialState);
        });
    });

    describe('Logout User Action', () => {
        it('Sets the username and session to null', () => {
            const user: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'Eric Lammers'
            };

            const resultState = userReducer(user, logoutUser());

            expect(resultState).toEqual(initialState);
        });
    });

    describe('Login Failed Action', () => {
        it('Sets the username and session to null', () => {
            const user: User = {
                session: 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
                userName: 'Eric Lammers'
            };

            const resultState = userReducer(user, logoutUser());

            expect(resultState).toEqual(initialState);
        });
    });
});