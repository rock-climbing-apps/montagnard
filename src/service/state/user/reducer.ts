import { User, UserActionTypes, LOGIN_USER, LOGOUT_USER, START_LOGIN, LOGIN_FAILED } from './types'

export const initialState: User = {
    userName: null,
    session: null,
};

export const userReducer = (state = initialState, action: UserActionTypes): User => {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...action.user,
            }
        case START_LOGIN: 
            return {
                userName: action.userName,
                session: null
            }
        
        case LOGOUT_USER:
        case LOGIN_FAILED:
            return {
                ...initialState,
            }
        default:
            return state
    }
}