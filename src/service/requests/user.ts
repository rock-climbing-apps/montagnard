// TODO - We are studding out the API calls
// eventually this will have to be updated to make real calls
import * as UserState from '../state/user/types';
import Users from '../../mock-data/users';

export interface UserLoginInfo {
    userName: string
    password: string
}

export interface User extends UserLoginInfo {
    email: string
    phoneNumber: number
    birthday: Date
}

export const authenticate = (userLoginInfo: UserLoginInfo, onSuccess: any, onError: any) : Promise<unknown> => {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            if (areCorrectCredentials(userLoginInfo)) {
                const user : UserState.User = {
                    userName: userLoginInfo.userName,
                    session: 'ahf4235khskfd3245h254',
                }
                
                resolve(user);
            }

            reject();
        }, 1500)
    }).then(onSuccess).catch(onError);
}

const areCorrectCredentials = (userLoginInfo : UserLoginInfo) : boolean => {
    if(!Users.getUser(userLoginInfo.userName)) {
        return false
    }

    if(Users.getUser(userLoginInfo.userName).password !== userLoginInfo.password) {
        return false;
    }

    return true;
}

export const addUser = (user : User, onSuccess : any, onError : any) : Promise<unknown> => {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            Users.addUser(user);
            resolve()
        }, 1500)
    }).then(onSuccess).catch(onError);
}