import { authenticate, UserLoginInfo } from './user';


describe('User Requests', () => {
    describe('Authentification', () => {
        it('Successful login returns the user information', async () => {
            const userLoginInfo: UserLoginInfo = {
                userName: 'EricLammers',
                password: 'Password'
            }

            const onSuccess = jest.fn();
            const onError = jest.fn();

            return authenticate(userLoginInfo, onSuccess, onError).then(data => {
                expect(onSuccess.mock.calls[0][0]).toEqual({
                    userName: 'EricLammers',
                    session: 'ahf4235khskfd3245h254'
                });
            });
        });

        it('Failed login calls onError', () => {
            const userLoginInfo: UserLoginInfo = {
                userName: 'Unknown',
                password: 'password'
            }

            const onSuccess = jest.fn();
            const onError = jest.fn();

            return authenticate(userLoginInfo, onSuccess, onError).then(data => {
                expect(onError.mock.calls.length).toBe(1);
            });
        });
    })
});