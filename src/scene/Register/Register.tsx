import React, { useState } from 'react';
import { View } from 'react-native';
import { Spinner } from 'native-base';
import BackgroundClimber from '../../component/BackgroundClimber/BackgroundClimber';
import WhiteTextInput from '../../component/WhiteTextInput/WhiteTextInput';
import WhiteButton from '../../component/WhiteButton/WhiteButton';
import { ROUTES } from '../../service/navigation/routes';
import { addUser } from '../../service/requests/user';
import { User } from '../../service/requests/user';

interface RegisterProps {
    navigation: any
}

const Register = ({navigation} : RegisterProps) => {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmedPassword, setConfirmPassword] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [email, setEmail] = useState('');
    const [birthday, setBirthday] = useState('');

    const [loading, setLoading] = useState(false);

    const optionallyDisplaySpinner = () => {
        if(loading) {
            return <Spinner color='white' />;
        }
    }

    // TODO - Need validation here
    const onRegisterPressed = () => {
        const onSuccess = () => {
            setLoading(false);
            navigation.navigate(ROUTES.SignIn);
        }
        const onError = () => setLoading(false);

        const user : User = {
            userName,
            password,
            email,
            phoneNumber,
            birthday
        }

        setLoading(true);
        addUser(user, onSuccess, onError);
    }

    return (
        <BackgroundClimber>
            <WhiteTextInput 
                placeholder="user name" 
                onChangeText={setUserName}
                editable={!loading}
            />
            <WhiteTextInput 
                placeholder="password" 
                onChangeText={setPassword}
                editable={!loading}
            />
            <WhiteTextInput 
                placeholder="confirm password" 
                onChangeText={setConfirmPassword}
                editable={!loading}
            />
            <WhiteTextInput 
                placeholder="phone number" 
                onChangeText={setPhoneNumber}
                editable={!loading}
            />
            <WhiteTextInput 
                placeholder="email" 
                onChangeText={setEmail}
                editable={!loading}
            />
            <WhiteTextInput 
                placeholder="birthday" 
                onChangeText={setBirthday}
                editable={!loading}
            />
            <WhiteButton
                text="Register"
                onPress={onRegisterPressed}
                buttonStyle={{ marginTop: 5, width: 300 }}
                disabled={loading}
            />
            <View style={{height: 50}}>
                {optionallyDisplaySpinner()}
            </View>
        </BackgroundClimber>
    );
}

export default Register;