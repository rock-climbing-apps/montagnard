import * as React from 'react';
import { NavigationScreenProp } from 'react-navigation';
import BackgroundClimber from '../../component/BackgroundClimber/BackgroundClimber';
import WhiteButton from '../../component/WhiteButton/WhiteButton';
import { ROUTES } from '../../service/navigation/routes';

interface AccountProps {
    navigation: NavigationScreenProp<any,any>,
}

const Account = ({navigation} : AccountProps) => {
    const onSignInPressed = () => navigation.navigate(ROUTES.SignIn);
    const onRegisterPressed = () => navigation.navigate(ROUTES.Register);

    return (
        <BackgroundClimber>
            <WhiteButton text="Sign In" onPress={onSignInPressed} />
            <WhiteButton text="Register" onPress={onRegisterPressed}  />
        </BackgroundClimber>
    );
};

export default Account;
