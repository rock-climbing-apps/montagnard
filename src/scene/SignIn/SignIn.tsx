import React, { useState } from 'react';
import { View, TextInput, StyleSheet, TextStyle } from 'react-native';
import { connect } from 'react-redux';
import { Spinner } from 'native-base';
import BackgroundClimber from '../../component/BackgroundClimber/BackgroundClimber';
import WhiteButton from '../../component/WhiteButton/WhiteButton';
import { AppState } from '../..//service/state/store';
import { loginUser } from "../../service/state/user/actions";
import { loggingIn } from '../../service/state/selector';

const styles = StyleSheet.create({
    textInput: {
        fontSize: 25,
        margin: 8,
        width: 250,
        backgroundColor: 'white',
        textAlign: 'center',
        padding: 10,
        paddingRight: 20,
        paddingLeft: 20,
        borderColor: 'black',
        borderRadius: 5,
    },
});

interface SignInProps {
    navigation: any
    attemptingLogin: boolean
    loggin: any
}

const SignIn = ({ attemptingLogin, navigation, loggin }: SignInProps) => {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const getTextInputStyle = () : Array<TextStyle> => ([
        styles.textInput,
        {
            opacity: attemptingLogin ? .5 : 1
        }
    ]);

    const optionallyDisplaySpinner = () => {
        if(attemptingLogin) {
            return <Spinner color='white' />;
        }
    }

    return (
        <BackgroundClimber>
            <TextInput
                style={getTextInputStyle()}
                placeholder="Username"
                onChangeText={(text) => setUserName(text)}
                editable={!attemptingLogin}
                autoCapitalize='none'
            />
            <TextInput
                style={getTextInputStyle()}
                placeholder="Password"
                onChangeText={(text) => setPassword(text)}
                editable={!attemptingLogin}
                autoCapitalize='none'
            />
            <WhiteButton
                text="Sign In"
                onPress={() => loggin({ userName: userName, password: password }, navigation)}
                buttonStyle={{ marginTop: 5 }}
                disabled={attemptingLogin}
            />
            <View style={{height: 50}}>
                {optionallyDisplaySpinner()}
            </View>
            
        </BackgroundClimber>
    )
};

const mapStateToProps = (state: AppState) => ({
    attemptingLogin: loggingIn(state),
});

const dispatchProps = {
    loggin: loginUser,
}

export default connect(mapStateToProps, dispatchProps)(SignIn);