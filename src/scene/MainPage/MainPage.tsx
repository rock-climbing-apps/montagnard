import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { connect } from 'react-redux';
import { AppState } from '../..//service/state/store';
import { loginUser } from "../../service/state/user/actions";
import { ROUTES } from '../../service/navigation/routes';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

interface MainPageProps {
    appState: AppState
    loggin: typeof loginUser,
    navigation: any
}

const MainPage = ({ appState, loggin, navigation }: MainPageProps) => {
    return (
        <View style={styles.container}>
            <Text>{JSON.stringify(appState)}</Text>
            <Button title="Login" onPress={
                () => loggin({ userName: 'EricLammers', password: 'password' }, navigation)
            } />
            <Button title="Sign In" onPress={() => navigation.navigate(ROUTES.SignIn)} />
        </View>
    );
}

const mapStateToProps = (state: AppState) => ({
    appState: state,
});

const dispatchProps = {
    loggin: loginUser,
}

export default connect(mapStateToProps, dispatchProps)(MainPage);